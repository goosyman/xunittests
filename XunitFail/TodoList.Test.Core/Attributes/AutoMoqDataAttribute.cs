﻿using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit2;
using TodoList.Test.Core.AutofixtureCustomizations;

namespace TodoList.Test.Core.Attributes
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute()
            : base(new Fixture()
                .Customize(new DomainCustomization()))
        {
        }
    }
}
