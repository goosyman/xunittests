using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace TodoList.Test.Core.AutofixtureCustomizations
{
    public class DomainCustomization : CompositeCustomization
    {
        public DomainCustomization() : base(new AutoMoqCustomization()) {}
    }
}