﻿using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit2;
using Test.Core.AutofixtureCustomizations;

namespace Test.Core.Attributes
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute()
            : base(new Fixture()
                .Customize(new DomainCustomization()))
        {
        }
    }
}