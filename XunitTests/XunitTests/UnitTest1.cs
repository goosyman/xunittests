﻿using Test.Core.Attributes;
using Xunit;

namespace XunitTests
{
    public class UnitTest1
    {
        [Theory]
        [AutoMoqData]
        public void TestMethod1(int x)
        {
            x = 5;
            Assert.Equal(5, x);
        }
    }
}
